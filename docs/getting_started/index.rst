===============
Getting started
===============

.. toctree::
   :maxdepth: 2

   install
   update
   quickstart
   how-to-join
   how-to-host
   how-to-manage
   troubleshooting

